import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

public class MiniMusic3 {

    static JFrame frame;
    static DrawingPanel panel;

    public static void main(String[] args) {
        MiniMusic3 mini = new MiniMusic3();
        mini.go();
    }

    public void go() {

        makeGui();

        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();

            int[] eventIWant = {127};
            player.addControllerEventListener(panel, eventIWant);

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track =  seq.createTrack();

            for(int i=5; i<60; i+=2) {
                int note = (int)(Math.random()*70);
                track.add(makeEvent(144, 7, note, 100, i));
                track.add(makeEvent(176, 1, 127, 0, i));
                track.add(makeEvent(128, 7, note, 100, i+2));
            }

            player.setSequence(seq);
            player.setTempoInBPM(220);
            player.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void makeGui() {

        frame = new JFrame("NoRhythm");
        panel = new DrawingPanel();
        frame.setContentPane(panel);
        frame.setBounds(30, 30, 1000, 1000);
        frame.setVisible(true);
    }

    public static MidiEvent makeEvent(int command, int channel, int note, int velocity, int tick) {
        MidiEvent event = null;

        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(command, channel, note, velocity);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }

    public class DrawingPanel extends JPanel implements ControllerEventListener{

        boolean msg = false;

        public void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;

            int red = (int) (Math.random()*256);
            int green = (int) (Math.random()*256);
            int blue = (int) (Math.random()*256);

            Color color = new Color(red, green, blue);

            g.setColor(color);

            int x = (int) (Math.random()*500);
            int y = (int) (Math.random()*500);
            int height = (int) (Math.random()*500+10);
            int width = (int) (Math.random()*500+10);

            g.fillRect(x, y, width, height);

            msg = false;

        }

        @Override
        public void controlChange(ShortMessage event) {
            msg = true;
            repaint();
        }
    }

}
